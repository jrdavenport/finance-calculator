export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const HOMEPAGE = '/homepage';
export const PROJECTOR = '/projector';
