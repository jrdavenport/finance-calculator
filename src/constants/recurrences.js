export const RECURRENCE_OPTIONS = [
  {description: null, value: null, disabled: true},
  {description: 'daily', value: 'daily' },
  {description: 'weekdays', value: 'weekdays' },
  {description: 'weekly', value: 'weekly' },
  {description: '4 weekly', value: '4 weekly' },
  {description: 'monthly', value: 'monthly' },
  {description: 'quaterly', value: 'quaterly'},
];
